// import CounterActions from "./components/CounterActions";
// import Counter from "./components/Counter";
import { connect } from 'react-redux'
import Header from "./components/Header";
import Footer from "./components/Footer";
import { routes } from './routes'
import { BrowserRouter as Router, Route, Link, Routes, Navigate } from 'react-router-dom';

const mapStateToProps = state => (
  {
    dark: state.site.dark,
    user: state.auth.user
  }
)

function App({ dark, user }) {
  return (
    <>
      <div className={dark ? 'dark' : 'ligth'}>
        <Header />
        <Routes>
          {routes.map((route, index) => (
            <Route path={route.path} key={index}
              element={
                route.auth && !user ? <Navigate to="/login" replace={true} /> : <route.component />
              } />
          ))}
        </Routes>
        <Footer />
      </div>
    </>
  );
}

export default connect(mapStateToProps)(App);
