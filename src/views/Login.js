import React from 'react'
import { useDispatch } from 'react-redux'
import { login } from '../stores/AuthSlice'
import { useNavigate } from "react-router-dom";
function Login() {
    const dispatch = useDispatch()
    let navigate = useNavigate();

    const handleLogin = () => {
        // request user
        const dummyData = {
            id: 1,
            name: 'yusuf',
            token: 'asdasdasd11221'
        }
        dispatch(login(dummyData))
        navigate('/')
    }
    return (
        <div style={{ background: 'red', padding: '10px' }}>
            <button onClick={handleLogin}> Giriş YAP</button>
        </div>

    )
}

export default Login