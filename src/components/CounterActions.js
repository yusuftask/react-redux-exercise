import React from 'react'
import { increment, decrement,incrementByAmount } from '../stores/CounterSlice'
import { useDispatch } from 'react-redux'
const CounterActions = () => {

    const dispatch = useDispatch()
    return (
        <div>
            <button onClick={() => dispatch(increment())}>Arttır (+)</button>
            <button onClick={() => dispatch(decrement())}>Azalt (-)</button>
            <button onClick={() => dispatch(incrementByAmount(4))}>4 arttır </button>
        </div>
    )
}

export default CounterActions