import React from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { setDarkMode, setLanguage } from '../stores/siteSlice'
import { logout } from '../stores/AuthSlice'
function Footer() {
    const { dark, language } = useSelector(state => state.site)
    const { user } = useSelector(state => state.auth)
    const dispatch = useDispatch()
    const languages = ['tr', 'en', 'de']
    const handleLanguage = lang => {
        dispatch(setLanguage(lang))
    }
    return (
        <div>
            Footer
            <div>
                {
                    languages.map((lang, index) => (
                        <button onClick={() => handleLanguage(lang)} className={lang === language ? 'active' : ''} key={index}>{lang}</button>
                    ))
                }
            </div>
            <div>
                <button onClick={() => dispatch(setDarkMode())}>
                    {dark ? 'Light mode' : 'Dark mode'}
                </button>
            </div>
            <div>
                {
                    user && (
                        <div>
                            <button onClick={() => dispatch(logout())}> Çıkış Yap</button>
                        </div>
                    )
                }
            </div>
        </div>
    )
}

export default Footer