import React from 'react'
import { useSelector } from 'react-redux'
import { NavLink } from 'react-router-dom'
function Header() {
    const { dark, language } = useSelector(state => state.site)
    return (
        <div>
            Header
            <nav>
                <NavLink exact="true" to="/" activeClassName="active">Anasayfa </NavLink>
                <NavLink exact="true" to="/about" activeClassName="active">Hakkında </NavLink>
                <NavLink exact="true" to="/profile" activeClassName="active">Profil </NavLink>
            </nav>
            <div>
                dark mode = {dark ? 'evet' : 'hayır'} <br />
                mevcut dil = {language}
            </div>
        </div>
    )
}

export default Header