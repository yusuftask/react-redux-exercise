import { configureStore } from '@reduxjs/toolkit'
import counterReducer from './CounterSlice'
import siteReducer from './siteSlice'
import authReducer from './AuthSlice'
export default configureStore({
    reducer: {
        count: counterReducer,
        site: siteReducer,
        auth: authReducer
    },
})